#!/bin/bash

set -eu

mkdir -p /run/directus/session /app/data/uploads /run/directus/logs /app/data/config /run/directus/cache

if [[ ! -f /app/data/config/project.php ]]; then
    echo "=> initializing project on first run"
    sed -e "s/##SECRET_KEY/$(pwgen -1s 32)/" -e "s/##PUBLIC_KEY/$(pwgen -1s 32)/" /app/pkg/project.php.template > /app/data/config/project.php

    # https://docs.directus.io/guides/cli.html#install-module
    echo "=> installing database"
    php bin/directus install:database -k project

    echo "=> creating admin user"
    php bin/directus install:install -e admin@cloudron.local -p changeme -t 'Directus Project' -k project
else
    php bin/directus db:upgrade -N project
fi

echo "==> Setting permissions"
chown -R www-data.www-data /run/directus /app/data

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
