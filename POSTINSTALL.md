This app is pre-setup with an admin account. The initial credentials are:
  
**Email**: admin@cloudron.local<br/>
**Password**: changeme<br/>

Please change the admin email and password immediately.

