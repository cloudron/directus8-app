FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=8.8.1
RUN curl -L https://github.com/directus/directus/archive/v${VERSION}.tar.gz | tar -zxvf - --strip-components=1 && \
    rm -rf /app/code/public/uploads && ln -s /app/data/uploads /app/code/public/uploads && \
    rm -rf /app/code/logs && ln -s /run/directus/logs /app/code/logs && \
    rm -rf /app/code/config && ln -s /app/data/config /app/code/config

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/directus.conf /etc/apache2/sites-enabled/directus.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod headers expires deflate mime dir rewrite setenvif

RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/directus/session && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/7.4/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/7.4/cli/conf.d/99-cloudron.ini

COPY start.sh project.php.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
