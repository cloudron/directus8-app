#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    Key = webdriver.Key,
    until = webdriver.until,
    Builder = require('selenium-webdriver').Builder;

var email = 'admin@cloudron.local',
    password = 'changeme';

describe('Application life cycle test', function () {
    this.timeout(0);

    var server, browser = new Builder().forBrowser('chrome').build();

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    var app;

    function exists(selector) {
        return browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    function visible(selector) {
        return exists(selector).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
        });
    }

    function login(callback) {
        browser.get('https://' + app.fqdn + '/admin/#/login').then(function () {
            return visible(by.xpath('//input[@type="email"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//input[@type="email"]')).sendKeys(email);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@type="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Sign In"]')).click();
        }).then(function () { // the ui remembers the last login view (!) so navigate back home
            browser.sleep(6000);
        }).then(function () {
            return browser.get('https://' + app.fqdn + '/admin/#/project/collections');
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//h1[text()="Collections"]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function createCollection(callback) {
        browser.get('https://' + app.fqdn + '/admin/#/project/settings/collections').then(function () {
            return visible(by.xpath('//button/span/i[text()="add"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//button/span/i[text()="add"]')).click();
        }).then(function () {
            return visible(by.xpath('//h2[text()="Create Collection"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//input[contains(@placeholder, "Enter collection name")]')).sendKeys('goodiebag');
        }).then(function () {
            return browser.findElement(by.xpath('//input[contains(@placeholder, "Enter collection name")]')).sendKeys(Key.ENTER);
        }).then(function () {
            return visible(by.xpath('//h1[text()="Goodiebag"]'));
        }).then(function () {
            callback();
        });
    }

    function checkCollection(callback) {
        browser.get('https://' + app.fqdn + '/admin/#/project/settings/collections').then(function () {
            return visible(by.xpath('//div[contains(text(), "goodiebag")]'));
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('create collection', createCollection);
    it('check collection', checkCollection);

    it('can restart app', function () {
        execSync('cloudron restart --app ' + app.id);
    });

    it('check collection', checkCollection);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('check collection', checkCollection);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('check collection', checkCollection);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('create collection', createCollection);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('check collection', checkCollection);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
